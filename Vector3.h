//
// Created by morgan on 5/5/18.
//

#ifndef OWNRAYTRACER_VECTOR3_H
#define OWNRAYTRACER_VECTOR3_H

#include <ostream>

class Vector3 {

public:
    Vector3();
    Vector3(float, float, float);
    float getX() const;
    float getY() const;
    float getZ() const;
    static Vector3 zero();
    Vector3 operator*(float);
    Vector3 operator/(float);
    Vector3 operator+(Vector3);
    Vector3 operator-(Vector3);
    Vector3 normalize();
    float operator%(Vector3 r);
    float angle(Vector3);
    float length() const;
    void print();
    void printAsColor();
    float distance(Vector3);
    Vector3 inverse();


private:
    float x;
    float y;
    float z;

};

struct Ray {
    Vector3 origin = Vector3::zero();
    Vector3 direction = Vector3::zero();

    Ray(){}

    Ray(Vector3 origin, Vector3 direction){
        this->direction = direction;
        this->origin = origin;
    }

    Vector3 getCoordinates(float t){
        return origin + direction * t;
    }


};

#endif //OWNRAYTRACER_VECTOR3_H
