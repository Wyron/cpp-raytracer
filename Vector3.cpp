//
// Created by morgan on 5/5/18.
//

#include <cmath>
#include <iostream>
#include "Vector3.h"


Vector3::Vector3() {
    x = 0;
    y = 0;
    z = 0;
}

Vector3::Vector3(float x, float y, float z){
    this->x = x;
    this->y = y;
    this->z = z;
}

float Vector3::getX() const{
    return x;
}

float Vector3::getY() const{
    return y;
}

float Vector3::getZ() const{
    return z;
}

Vector3 Vector3::zero() {
    return Vector3(0,0,0);
}

Vector3 Vector3::operator*(float s) {
    return Vector3(x*s, y*s, z*s);

}

Vector3 Vector3::operator+(Vector3 v) {
    return Vector3(x + v.getX(), y + v.getY(), z + v.getZ());
}

Vector3 Vector3::normalize() {
    return *this*(1 / sqrt(*this % *this)); // Normalize?

}

float Vector3::operator%(Vector3 r) {
    return x*r.x + y*r.y + z*r.z;
}

Vector3 Vector3::operator-(Vector3 v) {
    return Vector3(x - v.getX(), y - v.getY(), z - v.getZ());
}

float Vector3::length() const{
    return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

float Vector3::angle(Vector3 v){
    float angle = acos((*this % v) / (this->length() * v.length()));
    return angle * 180 / M_PI;
}

void Vector3::print() {
    std::cout << "Vector3(" << getX() << ", " << getY() << ", " << getZ() << ")" << std::endl;
}

void Vector3::printAsColor(){
    printf("#%x%x%x\n", (int)getX(), (int)getY(), (int)getZ());
}

float Vector3::distance(Vector3 v) {
    return sqrt((pow(getX() + v.getX(), 2) + pow(getY() + v.getY(), 2) + pow(getZ() + v.getZ(), 2)));
}

Vector3 Vector3::inverse() {
    this->x = -this->x;
    this->y = -this->y;
    this->z = -this->z;
    return *this;
}

Vector3 Vector3::operator/(float d) {
    return Vector3(x/d, y/d, z/d);
}
