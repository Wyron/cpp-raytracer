#include <iostream>
#include <math.h>
#include <limits>
#include <fstream>
#include <vector>
#include "Vector3.h"

// http://ambrsoft.com/TrigoCalc/Sphere/SpherLineIntersection_.htm
// https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-ray-tracing/implementing-the-raytracing-algorithm

class SceneObject{

public:
    // =0 means: Has to be overriden, effectively turning SceneObject into an abstract class
    virtual bool intersect(const Ray &ray, float &t_result) = 0;
    virtual Vector3 getNormal(Vector3 p) const = 0;
    Vector3 color = Vector3(255, 255, 255);
};

class Sphere : public SceneObject{

public:
    Vector3 origin;
    float radius = 0;

    Sphere(){
        color = Vector3(255, 255, 255);
        origin = Vector3::zero();
        radius = 0;
    }

    Sphere(Vector3 origin, float radius){
        this->color = Vector3(255, 255, 255);
        this->origin = origin;
        this->radius = radius;
    }

    Sphere(Vector3 origin, float radius, Vector3 color){
        this->color = color;
        this->origin = origin;
        this->radius = radius;
    }

    bool intersect(const Ray &ray, float &t_result) override {
        float x1 = ray.origin.getX();
        float x2 = ray.direction.getX() + ray.origin.getX();
        float x3 = origin.getX();

        float y1 = ray.origin.getY();
        float y2 = ray.direction.getY() + ray.origin.getY();
        float y3 = origin.getY();

        float z1 = ray.origin.getZ();
        float z2 = ray.direction.getZ() + ray.origin.getZ();
        float z3 = origin.getZ();

        float a = pow(x2 - x1, 2) + pow(y2 - y1, 2) + pow(z2 - z1, 2);
        float b = 2 * ((x2 - x1) * (x1 - x3) + (y2 - y1) * (y1 - y3) + (z2 - z1) * (z1 - z3));
        float c = pow(x3, 2) + pow(y3, 2) + pow(z3, 2) + pow(x1, 2) + pow(y1, 2) + pow(z1, 2) - 2 * (x3 * x1 + y3 * y1 + z3 * z1) - pow(radius, 2);

        float discr = pow(b, 2) - 4 * a * c;

        if(discr < 0){
            // no intersection
            return false;
        } else if (discr == 0){

            float t = -b/(2 * a);
            t_result = t;
            return true;
        } else {

            float t_plus = (-b + sqrt(discr)) / (2 * a);
            float t_minus = (-b - sqrt(discr)) / (2 * a);

            // Return the one thats closer to the rays origin, which is the smaller t
            if(t_plus < t_minus){
                t_result = t_plus;

            } else {
                t_result = t_minus;

            }

            // If the value is too close to zero, the shadowray hit the object itself
            if(t_result < 1e-5){
                t_result = std::numeric_limits<float>::max();
                return true;
            }
            return true;
        }
    }

    Vector3 getNormal(Vector3 p) const override{
        return (p - origin).normalize();
    }
};

struct Scene {

    std::vector<SceneObject*> sceneObjects;
    int currentIndex = 0;
    Sphere light;

    struct TraceResult {
        float t;
        SceneObject* sceneObject;
    };

    Scene(int numberOfSpheres){
        sceneObjects = std::vector<SceneObject*>(numberOfSpheres);
        light = Sphere(Vector3(-5, 4, 2.5f), 1, Vector3(0xff, 0xff, 0xff));
    }

    ~Scene(){
        std::cout << "Destroy Scene" << std::endl;
    }

    void addSphere(Sphere* sceneObject){
        sceneObjects[currentIndex++] = sceneObject;
    }

    TraceResult traceScene(const Ray& ray){
        float t_result = std::numeric_limits<float>::max();
        SceneObject* hitSceneObject = nullptr;

        for (SceneObject* sceneObject : sceneObjects) {
            float result;
            bool hasIntersection = sceneObject->intersect(ray, result);

            if(hasIntersection && result < t_result){
                t_result = result;
                hitSceneObject = sceneObject;
            }
        }

        return TraceResult{t_result, hitSceneObject};

    }

    Vector3 computeColor(Ray &ray, Vector3 p, SceneObject& hitSceneObject){

        Vector3 pointToLight = light.origin - p;
        Ray shadowRay(p, pointToLight);

        float t_light;

        if(light.intersect(shadowRay, t_light)){

            Scene::TraceResult traceResult = traceScene(shadowRay);
            float t_scene = traceResult.t;

            if(t_scene < t_light){
                // There is an object between the point and the light
                // Return black to indicate the shadow
                return Vector3::zero();
            } else {

                // Direct illumination
                Vector3 normal = hitSceneObject.getNormal(p);

//                float scalar = normal % shadowRay.direction.normalize();

//                float lambert_cos = normal % pointToLight.normalize();

                float angle = normal.angle(pointToLight.normalize());

                if(angle > 90.0f){
                    return Vector3(0, 0, 0);
                } else {
                    float scalar = 1 - angle / 90.0f; // Map angle to a value between [0, 1] and invert it
                    return hitSceneObject.color * scalar;
                }
            }
        }
    }
};

void raytrace(){

    constexpr int rangeX = 5;
    constexpr int rangeY = 5;

    constexpr float resolutionX = 512;
    constexpr float resolutionY = 512;

    std::ofstream out;
    out.open("out.ppm", std::ios::out | std::ios::binary);
    out << "P6 512 512 255 ";

    Scene scene(5);
    Sphere s(Vector3(0, 0, 5), 1.5f, Vector3(0xff, 0, 0));
    scene.addSphere(&s);
    Sphere s1(Vector3(2, 4, 5), 2, Vector3(0xff, 0, 0xff));
    scene.addSphere(&s1);
    Sphere s2(Vector3(2, -4, 5), 1.5f, Vector3(0, 0xff, 0xff));
    scene.addSphere(&s2);
    Sphere s3(Vector3(4, 0, 5), 1, Vector3(0xff, 0xff, 0));
    scene.addSphere(&s3);
    Sphere s4(Vector3(-3.5f, 0, 5), .5f, Vector3(0, 0xff, 0));
    scene.addSphere(&s4);

    for (float j = -rangeX; j < rangeX; j += (2*rangeX/resolutionX)) {
        for (float i = -rangeY; i < rangeY; i += (2*rangeY/resolutionY)) {

            Vector3 color(127, 127, 127);
            Ray ray(Vector3(j, i, 0), Vector3(0, 0, 1));

            Scene::TraceResult traceResult = scene.traceScene(ray);
            float t_scene = traceResult.t;
            SceneObject* hitSphere = traceResult.sceneObject;

            // If we hit something, t is not infinite and not NaN
            if(t_scene != std::numeric_limits<float>::max() || t_scene != t_scene) {
                Vector3 intersectionPoint = ray.getCoordinates(t_scene);
                color = scene.computeColor(ray, intersectionPoint, *hitSphere);
            }
            out << (char)color.getX() << (char)color.getY() << (char)color.getZ();
        }
    }

    out.close();
}

int main() {

    raytrace();

    return 0;
}
